const express = require('express')
const cors = require('cors')
var bodyParser = require('body-parser')
const admin = require('firebase-admin')
var serviceAccount = require('./manual-c9568-firebase-adminsdk-k7qce-b0fbd14cab.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://manual-c9568.firebaseio.com"
});

/******** ALL ROUTE DECLARATION ********/
const trad = require('./routes/trad')
/****************************************/

const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())



// Automatically allow cross-origin requests
app.use(cors({ origin: true }))

// Add middleware to authenticate requests
// app.use(myMiddleware);

// build multiple CRUD interfaces:
app.use('/trad', trad)

module.exports = app

app.listen(3001)
