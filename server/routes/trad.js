const express = require('express')
const router = express.Router()
const googleTranslate = require('google-translate')('AIzaSyDMbeXr8hCgg7EbiI0m6kr4fD07YlVhXLc')

router.get('/', function (req, res) {
    const data = {
        working: true
    }
    res.json(data);
})

router.post('/', function (req, res) {
    googleTranslate.translate(req.body.string, 'en', function (err, translation) {
        const data = {
            received: translation.originalText,
            translation: translation.translatedText
        }
        res.json(data)
    })
})

router.post('/detect', function (req, res) {
    googleTranslate.detectLanguage(req.body.string, function (err, detection) {
        res.json(detection)
        // =>  [{ language: "en", isReliable: false, confidence: 0.5714286, originalText: "Hello" }, ...]
    })
})

module.exports = router