const functions = require('firebase-functions')
const app = require('./app')

// Expose Express API as a single Cloud Function:
// exports.api = functions.https.onRequest(app)

const api = functions.https.onRequest((request, response) => {
    if (!request.path) {
        request.url = `/${request.url}` // prepend '/' to keep query params if any
    }
    return app(request, response)
})

module.exports = { api };