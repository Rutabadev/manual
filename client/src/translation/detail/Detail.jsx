import React, { Component } from 'react'
import './Detail.css'
import localeCode from 'iso-639-1/build/index'

class Detail extends Component {

    render() {
        const object = this.props.value;
        if (object === undefined) {
            return null
        }
        return (
            <div className="detail">
                <br />
                <p>Detected language: {localeCode.getName(object.language)}</p>
                <p>Confidence: {(object.confidence*100).toFixed(1)}%</p>
            </div>
        )
    }
}

export default Detail;