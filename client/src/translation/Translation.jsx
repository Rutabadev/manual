import React, { Component } from 'react'
import './Translation.css'
import matthieu from './matthieu.jpg'
import Detail from './detail/Detail'

class Translation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      string: '',
      detail: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      string: event.target.value
    })
  }

  handleSubmit(event) {
    this.setState({
      translation: '',
      detail: ''
    })

    event.preventDefault()
    if (this.state.string === 'Matthieu') {
      this.setState({
        translation: <img src={matthieu} alt="C'est Matthieu" />
      })
      return
    }
    this.setState({
      loadingTrad: true,
      loadingDetail: true,
    })

    fetch(process.env.REACT_APP_API_HOST + '/trad', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        string: this.state.string,
      })
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState({
          translation: data.translation,
          loadingTrad: false
        })
      })

    fetch(process.env.REACT_APP_API_HOST + '/trad/detect', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        string: this.state.string,
      })
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState({
          detail: { data },
          loadingDetail: false
        })
      })
  }

  render() {
    const trad = () => {
      if (this.state.loadingTrad && this.state.loadingDetail) {
        return (<div className="spinner"></div>)
      } else {
        return (
          <div>
            <p>{this.state.translation}</p>
            <Detail value={this.state.detail.data} />
          </div>
        )
      }
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="content">
          <input type="text" className="niceInput" value={this.state.string} onChange={this.handleChange} />
          <button type="submit" className="niceButton">Translate</button>
          <br />
          {trad()}
        </div>
      </form>
    );
  }
}

export default Translation;
