import React, { Component } from 'react';
import './App.css';
import Translation from './translation/Translation';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Translation/>
      </div>
    );
  }
}

export default App;
